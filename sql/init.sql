CREATE TABLE IF NOT EXISTS page_views (
	id uuid NOT NULL,
	page_id varchar(100) NOT NULL,
	user_agent varchar(150) NULL,
	browser varchar(50) NULL,
	client_ip inet NULL,
	client_country varchar(100) NULL,
	user_id varchar(200) NULL,
	"timestamp" timestamp NOT NULL,
	CONSTRAINT pageviews_pk PRIMARY KEY (id)
);
CREATE INDEX IF NOT EXISTS pageviews_page_id_idx ON page_views (page_id);
CREATE INDEX IF NOT EXISTS pageviews_timestamp_idx ON page_views ("timestamp");
CREATE INDEX IF NOT EXISTS pageviews_browser_idx ON page_views (browser);
CREATE INDEX IF NOT EXISTS pageviews_user_id_idx ON page_views (user_id);

-----------------------------
CREATE OR REPLACE FUNCTION fn_getuserrate() returns float4
	LANGUAGE 'plpgsql' VOLATILE
AS $$
	DECLARE
		_totalUsers int;
		_returningUsers int;
 		_rate float4;
	BEGIN
		SELECT count(1) INTO _returningUsers FROM (SELECT 1 FROM page_views pv
		GROUP BY pv.user_id having count(*) > 1) as cnt;

		SELECT count(DISTINCT pv.user_id) INTO _totalUsers FROM page_views pv;
		IF _totalUsers = 0 THEN
			_rate := 0;
		ELSE
			_rate := CAST(_returningUsers AS float4)/CAST(_totalUsers AS float4);
		END IF;
		RETURN _rate;
	END;
$$
;
