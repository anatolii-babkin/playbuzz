import { getCountryByIp } from "../src/utils";

describe("Country resolution", () => {
  it("get null for loopback", async () => {
    const result = await getCountryByIp("127.0.0.1");
    expect(result).toBe(null);
  });

  it("get USA for www.usa.gov ipv4", async () => {
    const result = await getCountryByIp("13.225.249.121");
    expect(result).toBe("United States");
  });

  it("get USA for www.usa.gov ipv6", async () => {
    const result = await getCountryByIp("2600:9000:21f8:a00:1c:3bbe:c80:93a1");
    expect(result).toBe("United States");
  });
});
