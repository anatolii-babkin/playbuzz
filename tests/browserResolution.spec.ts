import { getUserAgent } from "../src/utils";

describe("Country resolution", () => {
  it("get null for invalid UA", async () => {
    const result = getUserAgent("invalid-ua");
    expect(result).toBe(null);
  });

  it("should detect chrome", async () => {
    const result = getUserAgent(
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
    );
    expect(result).toBe("Chrome");
  });

  it("should detect firefox", async () => {
    const result = getUserAgent(
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0"
    );
    expect(result).toBe("Firefox");
  });

  it("should detect edge", async () => {
    const result = getUserAgent(
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18363"
    );
    expect(result).toBe("Edge");
  });
});
