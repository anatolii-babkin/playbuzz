import {
  validateTrackPageViewRequest,
  validateId,
  validatePageViewsByBrowserRequest
} from "../src/models";

describe("Track Page View Validator", () => {
  it("should pass validation with minimal data", async () => {
    const result = await validateTrackPageViewRequest({
      pageId: "some-pageId"
    });
    expect(result.isValid).toBe(true);
  });

  it("should pass validation with full data", async () => {
    const result = await validateTrackPageViewRequest({
      pageId: "some-pageId",
      clientIp: "192.168.1.1",
      userAgent: "Chrome",
      userId: "some_user",
      timeStamp: new Date()
    });
    expect(result.isValid).toBe(true);
  });

  it("should reject missing pageid", async () => {
    const result = await validateTrackPageViewRequest({
      pageId: ""
    });
    expect(result.isValid).toBe(false);
    expect(result.errors).toHaveProperty("pageId");
  });

  it("should reject invalid ip", async () => {
    const result = await validateTrackPageViewRequest({
      pageId: "some-pageId",
      clientIp: "invalid-ip"
    });
    expect(result.isValid).toBe(false);
    expect(result.errors).toHaveProperty("clientIp");
  });

  it("should reject long data ip", async () => {
    const longString = [...new Array(250).keys()].join();
    const result = await validateTrackPageViewRequest({
      pageId: longString,
      userAgent: longString,
      userId: longString
    });
    expect(result.isValid).toBe(false);
    expect(result.errors).toHaveProperty("pageId");
    expect(result.errors).toHaveProperty("userAgent");
    expect(result.errors).toHaveProperty("userId");
  });
});

describe("Page View Id Validator", () => {
  it("should pass", async () => {
    const result = await validateId("d2bc93e4-433a-4cc2-92fd-1177c5227ce9");
    expect(result.isValid).toBe(true);
  });

  it("should fail on empty", async () => {
    const result = await validateId("");
    expect(result.isValid).toBe(false);
  });

  it("should fail on invalid uuid", async () => {
    const result = await validateId("d2bc93e4-433a-4cc2-92fd-1177c52");
    expect(result.isValid).toBe(false);
  });
});

describe("Page View By Browser Validator", () => {
  it("should pass", async () => {
    const result = await validatePageViewsByBrowserRequest({
      browser: "Chrome",
      skip: 0,
      take: 10
    });
    expect(result.isValid).toBe(true);
  });

  it("should fail on negative skip", async () => {
    const result = await validatePageViewsByBrowserRequest({
      browser: "Chrome",
      skip: -1,
      take: 10
    });
    expect(result.isValid).toBe(false);
  });

  it("should fail on negative take", async () => {
    const result = await validatePageViewsByBrowserRequest({
      browser: "Chrome",
      skip: 1,
      take: -10
    });
    expect(result.isValid).toBe(false);
  });

  it("should fail on zero take", async () => {
    const result = await validatePageViewsByBrowserRequest({
      browser: "Chrome",
      skip: 1,
      take: 0
    });
    expect(result.isValid).toBe(false);
  });

  it("should fail on missing browser", async () => {
    const result = await validatePageViewsByBrowserRequest({
      browser: "",
      skip: 1,
      take: 10
    });
    expect(result.isValid).toBe(false);
  });

  it("should fail on large take", async () => {
    const result = await validatePageViewsByBrowserRequest({
      browser: "",
      skip: 1,
      take: 10000
    });
    expect(result.isValid).toBe(false);
  });
});
