# playbuzz

### Prerequisites
[Docker](https://www.docker.com/)

### Launch application
Use this commands to launch the application:
```bash
cd playbuzz
docker-compose up --build
```
Then go to [http://localhost:3000](http://localhost:3000)

### Examples
Create page view:
```bash
curl -X POST "http://localhost:3000/api/v1/page-view" -H "accept: application/json" \
-H "Authorization: 6i2nSgWu0DfYIE8I0ZBJOtxTmHJATRzu" -H "Content-Type: application/json" \
-d "{\"pageId\":\"profile\",\"userAgent\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\", \
\"clientIp\":\"194.183.183.122\",\"userId\":\"solaire@astora.com\",\"timeStamp\":\"2020-01-03T21:12:07.214Z\"}"
```

Get page view by ID:
```bash
curl -X GET "http://localhost:3000/api/v1/page-view/6da978ea-6b62-406f-bd4d-a3b370d2f9b7" \
-H "accept: application/json" -H "Authorization: 6i2nSgWu0DfYIE8I0ZBJOtxTmHJATRzu"
```

For other examples refer to swagger doc