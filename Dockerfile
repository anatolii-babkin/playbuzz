FROM node:12.10-alpine

RUN apk add --update postgresql-client
COPY scripts/wait-for-postgres.sh /usr/scripts/
RUN chmod a+x /usr/scripts/wait-for-postgres.sh

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build:all
EXPOSE 3000
CMD [ "/usr/scripts/wait-for-postgres.sh", "node", "./build/app.js" ]