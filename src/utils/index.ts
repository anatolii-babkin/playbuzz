import axios from "axios";
// eslint-disable-next-line no-unused-vars
import { Schema, ValidationError as YupValidationError } from "yup";
import { UAParser } from "ua-parser-js";
import { ipstackKey } from "../config";

export type ValidationErrors = { [key: string]: string };
export interface ValidationResult {
  isValid: boolean;
  errors?: ValidationErrors;
}

export async function validateModel<T>(value: T, schema: Schema<T>): Promise<ValidationResult> {
  try {
    await schema.validate(value, { abortEarly: false });
    return {
      isValid: true
    };
  } catch (e) {
    const errors: ValidationErrors = {};
    const error = e as YupValidationError;
    if (error.inner) {
      for (const inner of error.inner) {
        errors[inner.path] = inner.message;
      }
    } else {
      errors[error.path] = error.message;
    }
    return {
      isValid: false,
      errors
    };
  }
}

export async function getCountryByIp(ip?: string): Promise<string | null> {
  if (!ip) {
    return Promise.resolve(null);
  }

  const url = `http://api.ipstack.com/${ip.trim()}?access_key=${ipstackKey}`;
  try {
    const response = await axios.get(url);
    if (response.data.success === false) {
      throw new Error("non-succes response from ipstack country resolution");
    }
    return response.data.country_name;
  } catch (e) {
    console.error(`Failed to get country by ip '${ip}'`, e);
    return null;
  }
}

export function getUserAgent(userAgent?: string): string | null {
  if (!userAgent) {
    return null;
  }
  const parser = new UAParser(userAgent.trim());
  return parser.getBrowser().name || null;
}
