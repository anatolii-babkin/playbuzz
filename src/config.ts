import dotenv from "dotenv";

if (process.env.NODE_ENV !== "production") {
  dotenv.config();
}

export const apiPort = process.env.API_PORT as string;
export const securityToken = process.env.API_KEY as string;
export const ipstackKey = process.env.IPSTACK_KEY as string;
export const dbHost = process.env.POSTGRES_HOST as string;
export const dbUser = process.env.POSTGRES_USER as string;
export const dbPassword = process.env.POSTGRES_PASSWORD as string;
export const dbPort = parseInt(process.env.POSTGRES_PORT || "", 10);
export const dbDatabase = process.env.POSTGRES_DB as string;
