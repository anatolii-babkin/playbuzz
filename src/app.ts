import express from "express";
import swaggerUI from "swagger-ui-express";
import { RegisterRoutes } from "./routes";
import { apiPort } from "./config";
import storage from "./storage";
const swaggerDoc = require("../swagger/swagger.json");

storage
  .init()
  .then(() => console.info("DB initialized"))
  .catch(error => console.error("Failed to initialize DB", error));

const app = express();
app.use(express.json());

/* eslint-disable no-unused-vars */
// suppress 304 for /api* routes
app.get("/api*", (req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.setHeader("Last-Modified", new Date().toUTCString());
  next();
});
app.set("etag", false);
RegisterRoutes(app);
app.use("/", swaggerUI.serve, swaggerUI.setup(swaggerDoc));
app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
  if (err && err.status === 401) {
    res.status(401).send("Unauthorized");
  } else {
    // log this somewhere
    console.error("Unhandled error", err);
    res.status(500).send("Internal server error");
  }
});
/* eslint-enable no-unused-vars */
app.listen(apiPort, () => console.log(`Server started listening on port ${apiPort}`));
