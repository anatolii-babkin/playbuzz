// eslint-disable-next-line no-unused-vars
import express from "express";
import { securityToken } from "./config";

export function expressAuthentication(
  request: express.Request,
  securityName: string,
  // eslint-disable-next-line no-unused-vars
  scopes?: string[]
): Promise<any> {
  if (securityName === "api_key") {
    let token;
    if (request.headers && request.headers.authorization) {
      token = request.headers.authorization;
    }

    if (token === securityToken) {
      return Promise.resolve({
        authorized: true
      });
    }
  }
  return Promise.reject({});
}
