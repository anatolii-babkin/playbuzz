import * as yup from "yup";
import _ from "lodash";
/* eslint-disable no-unused-vars */
import { BasePageRequest, pagingRequestBaseValidationSchema } from "./basePageRequest";
import { validateModel, ValidationResult } from "../utils";
/* eslint-enable no-unused-vars */

export interface PageViewsByBrowserRequest extends BasePageRequest {
  browser: string;
}

const pageViewsByBrowserRequestSchema = yup
  .object<PageViewsByBrowserRequest>()
  .shape<PageViewsByBrowserRequest>({
    browser: yup
      .string()
      .trim()
      .required()
      .max(100),
    ...pagingRequestBaseValidationSchema
  });

export const validatePageViewsByBrowserRequest = _.partialRight<
  PageViewsByBrowserRequest,
  yup.Schema<PageViewsByBrowserRequest>,
  Promise<ValidationResult>
>(validateModel, pageViewsByBrowserRequestSchema);
