/* eslint-disable no-unused-vars */
import { PageView } from "./pageView";
import { CountPageViewsResponse } from "./countPageViewsResponse";
/* eslint-enable no-unused-vars */

export interface PageViewsResponse extends CountPageViewsResponse {
  pageViews: PageView[];
}
