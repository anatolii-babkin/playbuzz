import * as yup from "yup";
import isIp from "is-ip";
import _ from "lodash";
// eslint-disable-next-line no-unused-vars
import { validateModel, ValidationResult } from "../utils";

export interface TrackPageViewRequest {
  pageId: string;
  userAgent?: string;
  clientIp?: string;
  userId?: string;
  timeStamp?: Date;
}

const trackPageViewRequestSchema = yup.object<TrackPageViewRequest>().shape<TrackPageViewRequest>({
  pageId: yup
    .string()
    .trim()
    .required()
    .max(100),
  userAgent: yup
    .string()
    .trim()
    .max(150),
  clientIp: yup
    .string()
    .trim()
    .test({
      name: "IP",
      message: "clientIp field is invalid",
      test: v => (v ? isIp(v) : true)
    }),
  userId: yup
    .string()
    .trim()
    .max(200),
  timeStamp: yup.date()
});

export const validateTrackPageViewRequest = _.partialRight<
  TrackPageViewRequest,
  yup.Schema<TrackPageViewRequest>,
  Promise<ValidationResult>
>(validateModel, trackPageViewRequestSchema);
