import * as yup from "yup";
import _ from "lodash";
/* eslint-disable no-unused-vars */
import { validateModel, ValidationResult } from "../utils";
/* eslint-enable no-unused-vars */

export interface PageViewsCountByCountryRequest {
  country: string;
}

const pageViewsCountByCountryRequestSchema = yup
  .object<PageViewsCountByCountryRequest>()
  .shape<PageViewsCountByCountryRequest>({
    country: yup
      .string()
      .trim()
      .required()
      .max(100)
  });

export const validatePageViewsCountByCountryRequest = _.partialRight<
  PageViewsCountByCountryRequest,
  yup.Schema<PageViewsCountByCountryRequest>,
  Promise<ValidationResult>
>(validateModel, pageViewsCountByCountryRequestSchema);
