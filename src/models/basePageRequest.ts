import * as yup from "yup";
export interface BasePageRequest {
  skip: number;
  take: number;
}

export const pagingRequestBaseValidationSchema = {
  skip: yup
    .number()
    .required()
    .integer()
    .min(0),
  take: yup
    .number()
    .required()
    .integer()
    .min(1)
    .max(100)
};
