export * from "./pageView";
export * from "./trackPageViewRequest";
export * from "./pageViewsByBrowserRequest";
export * from "./pageViewsResponse";
export * from "./pageViewsByCountryRequest";
export * from "./countPageViewsResponse";
export * from "./pageViewsCountByCountryRequest";
export * from "./userRateResponse";

export const defaultSkip = 0;
export const defaultTake = 20;
