import * as yup from "yup";
import _ from "lodash";
/* eslint-disable no-unused-vars */
import { BasePageRequest, pagingRequestBaseValidationSchema } from "./basePageRequest";
import { validateModel, ValidationResult } from "../utils";
/* eslint-enable no-unused-vars */

export interface PageViewsByCountryRequest extends BasePageRequest {
  country: string;
}

const pageViewsByCountryRequestSchema = yup
  .object<PageViewsByCountryRequest>()
  .shape<PageViewsByCountryRequest>({
    country: yup
      .string()
      .trim()
      .required()
      .max(100),
    ...pagingRequestBaseValidationSchema
  });

export const validatePageViewsByCountryRequest = _.partialRight<
  PageViewsByCountryRequest,
  yup.Schema<PageViewsByCountryRequest>,
  Promise<ValidationResult>
>(validateModel, pageViewsByCountryRequestSchema);
