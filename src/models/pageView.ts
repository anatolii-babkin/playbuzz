import * as yup from "yup";
import _ from "lodash";
/* eslint-disable no-unused-vars */
import { PageViewData } from "../storage";
import { validateModel, ValidationResult } from "../utils";
/* eslint-enable no-unused-vars */

export interface PageView {
  id: string;
  pageId: string;
  userAgent?: string;
  browser?: string;
  clientIp?: string;
  clientCountry?: string;
  userId?: string;
  timeStamp: Date;
}

export const toPageView = (pageView?: PageViewData): PageView | null => {
  if (!pageView) {
    return null;
  }
  const result: PageView = {
    id: pageView.id,
    pageId: pageView.pageId,
    timeStamp: pageView.timeStamp
  };
  if (!_.isNil(pageView.userAgent)) {
    result.userAgent = pageView.userAgent;
  }
  if (!_.isNil(pageView.browser)) {
    result.browser = pageView.browser;
  }
  if (!_.isNil(pageView.clientIp)) {
    result.clientIp = pageView.clientIp;
  }
  if (!_.isNil(pageView.clientCountry)) {
    result.clientCountry = pageView.clientCountry;
  }
  if (!_.isNil(pageView.userId)) {
    result.userId = pageView.userId;
  }
  return result;
};

export const toPageViews = (pageView: PageViewData[]): PageView[] =>
  pageView.map(p => toPageView(p) as PageView);

const uuidRegexp = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
const idSchema = yup.object<{ id: string }>().shape<{ id: string }>({
  id: yup
    .string()
    .required()
    .test({
      name: "uuid",
      message: "id field is invalid",
      test: v => uuidRegexp.test(v)
    })
});

const validateIdObject = _.partialRight<
  { id: string },
  yup.Schema<{ id: string }>,
  Promise<ValidationResult>
>(validateModel, idSchema);

export const validateId = (id: string) => validateIdObject({ id });
