/* eslint-disable no-unused-vars */
import { Client } from "pg";
import sequelize, { Sequelize, DataTypes } from "sequelize";
import { PageViewRepository, PageViewData } from "./types";
import { PageViewModel } from "./PageViewModel";
/* eslint-enable no-unused-vars */
import "./requireSql";

export interface PgConfig {
  host: string;
  user: string;
  password: string;
  port: number;
  database: string;
}

type CountResult = { count: number };
type RateResult = { rate: number };
const initScript = require("../../sql/init.sql");

export class PgPageViewRepository implements PageViewRepository {
  private _config: PgConfig;
  private _sequelize: Sequelize;
  constructor(config: PgConfig) {
    this._sequelize = new Sequelize({
      dialect: "postgres",
      host: config.host,
      password: config.password,
      username: config.user,
      port: config.port,
      database: config.database
    });
    this._config = config;
    PageViewModel.init(
      {
        id: { field: "id", type: DataTypes.UUID, allowNull: false, primaryKey: true },
        pageId: { field: "page_id", type: DataTypes.STRING(100), allowNull: false },
        userAgent: { field: "user_agent", type: DataTypes.STRING(150), allowNull: true },
        browser: { field: "browser", type: DataTypes.STRING(50), allowNull: true },
        clientIp: { field: "client_ip", type: DataTypes.INET, allowNull: true },
        clientCountry: { field: "client_country", type: DataTypes.STRING(100), allowNull: true },
        userId: { field: "user_id", type: DataTypes.STRING(200), allowNull: true },
        timeStamp: { field: "timestamp", type: "timestamp", allowNull: true }
      },
      { sequelize: this._sequelize, tableName: "page_views", timestamps: false }
    );
  }
  async init() {
    await this._execute(initScript);
  }

  async create(data: PageViewData) {
    await PageViewModel.create(data);
  }

  async get(id: string) {
    const pageView = await PageViewModel.findOne({
      where: {
        id
      }
    });
    return PgPageViewRepository.toPageViewData(pageView);
  }

  async getByBrowser(browser: string, skip: number, take: number) {
    const pageViews = await PageViewModel.findAll({
      where: {
        browser
      },
      order: [["timestamp", "DESC"]],
      offset: skip,
      limit: take
    });
    return PgPageViewRepository.mapToPageViewData(pageViews);
  }

  async countByBrowser(browser: string) {
    const result = await PageViewModel.findAll({
      attributes: [[sequelize.fn("COUNT", 1), "count"]],
      where: {
        browser
      }
    });
    return (result[0].get() as CountResult).count;
  }

  async getByCountry(country: string, skip: number, take: number) {
    const pageViews = await PageViewModel.findAll({
      where: {
        clientCountry: country
      },
      order: [["timestamp", "DESC"]],
      offset: skip,
      limit: take
    });
    return PgPageViewRepository.mapToPageViewData(pageViews);
  }

  async countByCountry(country: string) {
    const result = await PageViewModel.findAll({
      attributes: [[sequelize.fn("COUNT", 1), "count"]],
      where: {
        clientCountry: country
      }
    });
    return (result[0].get() as CountResult).count;
  }

  async getUserRate() {
    const result = await this._sequelize.query("SELECT fn_getuserrate() as rate", {
      type: sequelize.QueryTypes.SELECT
    });
    return (result[0] as RateResult).rate;
  }

  private static toPageViewData(pageView: PageViewModel | null): PageViewData | null {
    if (pageView) {
      return pageView.get() as PageViewData;
    }
    return null;
  }

  private static mapToPageViewData(pageView: PageViewModel[]): PageViewData[] {
    return pageView.map(p => p.get() as PageViewData);
  }

  private async _execute<T>(queryText: string, values?: any[]) {
    const client = new Client(this._config);
    await client.connect();
    try {
      return await client.query<T>({
        text: queryText,
        values
      });
    } finally {
      await client.end();
    }
  }
}
