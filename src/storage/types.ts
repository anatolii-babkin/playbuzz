export interface PageViewData {
  id: string;
  pageId: string;
  userAgent?: string | null;
  browser?: string | null;
  clientIp?: string | null;
  clientCountry?: string | null;
  userId?: string | null;
  timeStamp: Date;
}

export interface PageViewRepository {
  init: () => Promise<any>;
  create: (data: PageViewData) => Promise<any>;
  get: (id: string) => Promise<PageViewData | null>;
  getByBrowser: (browser: string, skip: number, take: number) => Promise<PageViewData[]>;
  countByBrowser: (browser: string) => Promise<number>;
  getByCountry: (country: string, skip: number, take: number) => Promise<PageViewData[]>;
  countByCountry: (country: string) => Promise<number>;
  getUserRate: () => Promise<number>;
}
