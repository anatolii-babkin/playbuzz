import { PgPageViewRepository } from "./PgPageViewRepository";
// eslint-disable-next-line no-unused-vars
import { PageViewRepository } from "./types";
import { dbHost, dbPort, dbPassword, dbUser, dbDatabase } from "../config";

const pageViewRepository: PageViewRepository = new PgPageViewRepository({
  host: dbHost,
  port: dbPort,
  user: dbUser,
  password: dbPassword,
  database: dbDatabase
});

export * from "./types";

export default pageViewRepository;
