import _ from "lodash";
import { Controller, Security, Response, Get, Query, Route } from "tsoa";
import pageViewRepository from "../storage";
/* eslint-disable no-unused-vars */
import {
  validatePageViewsByCountryRequest,
  validatePageViewsCountByCountryRequest,
  CountPageViewsResponse,
  defaultSkip,
  defaultTake,
  PageViewsResponse,
  toPageViews
} from "../models";
import { ValidationErrors } from "../utils";
/* eslint-enable no-unused-vars */

@Route("")
export class CountriesController extends Controller {
  /**
   * Get page views by country
   */
  @Get("country/{country}/page-views/count")
  @Security("api_key")
  @Response<CountPageViewsResponse>(200, "Ok")
  @Response<ValidationErrors>(400, "Bad request")
  @Response(401, "Unauthorized")
  public async getPageViewsCountByCountry(country: string) {
    const request = {
      country
    };
    const validationResult = await validatePageViewsCountByCountryRequest(request);
    if (!validationResult.isValid) {
      this.setStatus(400);
      return validationResult.errors;
    }

    const result: CountPageViewsResponse = {
      count: await pageViewRepository.countByCountry(request.country)
    };
    return result;
  }

  /**
   * Get page views by country
   */
  @Get("country/{country}/page-views")
  @Security("api_key")
  @Response<PageViewsResponse>(200, "Ok")
  @Response<ValidationErrors>(400, "Bad request")
  @Response(401, "Unauthorized")
  public async getPageViewsByCountry(
    country: string,
    @Query() skip?: number,
    @Query() take?: number
  ) {
    const request = {
      country,
      skip: _.isNil(skip) ? defaultSkip : skip,
      take: _.isNil(take) ? defaultTake : take
    };
    const validationResult = await validatePageViewsByCountryRequest(request);
    if (!validationResult.isValid) {
      this.setStatus(400);
      return validationResult.errors;
    }

    const itemsPromise = pageViewRepository.getByCountry(
      request.country,
      request.skip,
      request.take
    );
    const countPromise = pageViewRepository.countByCountry(request.country);

    const values = await Promise.all([itemsPromise, countPromise]);
    const result: PageViewsResponse = {
      count: values[1],
      pageViews: toPageViews(values[0])
    };
    return result;
  }
}
