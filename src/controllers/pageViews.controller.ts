import uuidv4 from "uuid/v4";
import { Controller, Route, Security, Post, Body, Response, Get } from "tsoa";
import pageViewRepository from "../storage";
/* eslint-disable no-unused-vars */
import {
  validateTrackPageViewRequest,
  TrackPageViewRequest,
  PageView,
  toPageView,
  validateId
} from "../models";
import { ValidationErrors, getCountryByIp, getUserAgent } from "../utils";
/* eslint-enable no-unused-vars */

@Route("")
export class PageViewsController extends Controller {
  /**
   * Create page view
   */
  @Post("page-view")
  @Security("api_key")
  @Response<PageView>(201, "Created")
  @Response<ValidationErrors>(400, "Bad request")
  @Response(401, "Unauthorized")
  public async trackPageView(@Body() request: TrackPageViewRequest) {
    const validationResult = await validateTrackPageViewRequest(request);
    if (!validationResult.isValid) {
      this.setStatus(400);
      return validationResult.errors;
    }

    const data = {
      id: uuidv4(),
      pageId: request.pageId,
      userAgent: request.userAgent,
      browser: getUserAgent(request.userAgent),
      clientIp: request.clientIp,
      clientCountry: await getCountryByIp(request.clientIp),
      userId: request.userId,
      timeStamp: request.timeStamp || new Date()
    };
    await pageViewRepository.create(data);
    this.setStatus(201);
    return toPageView(data);
  }

  /**
   * Get page view by id
   */
  @Get("page-view/{id}")
  @Security("api_key")
  @Response<PageView>(200, "Ok")
  @Response<ValidationErrors>(400, "Bad request")
  @Response(401, "Unauthorized")
  @Response(404, "Not found")
  public async getPageView(id: string) {
    const validationResult = await validateId(id);
    if (!validationResult.isValid) {
      this.setStatus(400);
      return validationResult.errors;
    }

    const res = await pageViewRepository.get(id);
    if (!res) {
      this.setStatus(404);
      return null;
    }
    return toPageView(res);
  }
}
