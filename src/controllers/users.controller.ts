import { Controller, Security, Response, Get, Route } from "tsoa";
import pageViewRepository from "../storage";
/* eslint-disable no-unused-vars */
import { UserRateResponse } from "../models";
/* eslint-enable no-unused-vars */

@Route("")
export class UsersController extends Controller {
  /**
   * Get the ​rate​ between the number returning users out of all the unique users
   */
  @Get("users/rate")
  @Security("api_key")
  @Response<UserRateResponse>(200, "Ok")
  @Response(401, "Unauthorized")
  public async getUsersRate() {
    const result: UserRateResponse = {
      rate: await pageViewRepository.getUserRate()
    };
    return result;
  }
}
