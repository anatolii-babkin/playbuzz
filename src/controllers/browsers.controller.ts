import _ from "lodash";
import { Controller, Security, Response, Get, Query, Route } from "tsoa";
import pageViewRepository from "../storage";
/* eslint-disable no-unused-vars */
import {
  validatePageViewsByBrowserRequest,
  defaultSkip,
  defaultTake,
  PageViewsResponse,
  toPageViews
} from "../models";
import { ValidationErrors } from "../utils";
/* eslint-enable no-unused-vars */

@Route("")
export class BrowsersController extends Controller {
  /**
   * Get page views by browser name
   */
  @Get("browser/{browser}/page-views")
  @Security("api_key")
  @Response<PageViewsResponse>(200, "Ok")
  @Response<ValidationErrors>(400, "Bad request")
  @Response(401, "Unauthorized")
  public async getPageViewsByBrowser(
    browser: string,
    @Query() skip?: number,
    @Query() take?: number
  ) {
    const request = {
      browser,
      skip: _.isNil(skip) ? defaultSkip : skip,
      take: _.isNil(take) ? defaultTake : take
    };
    const validationResult = await validatePageViewsByBrowserRequest(request);
    if (!validationResult.isValid) {
      this.setStatus(400);
      return validationResult.errors;
    }

    const itemsPromise = pageViewRepository.getByBrowser(
      request.browser,
      request.skip,
      request.take
    );
    const countPromise = pageViewRepository.countByBrowser(request.browser);

    const values = await Promise.all([itemsPromise, countPromise]);
    const result: PageViewsResponse = {
      count: values[1],
      pageViews: toPageViews(values[0])
    };
    return result;
  }
}
